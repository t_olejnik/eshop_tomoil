﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using utb_eshop_patek.Infrastructure.Data;

namespace utb_eshop_patek.Infrastructure.Configuration
{
    public class InfrastructureServiceConfiguration
    {
        public static void Load(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("UTB.Eshop");
            });
        }
    }
}
