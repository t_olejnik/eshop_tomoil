﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using utb_eshop_patek.Infrastructure.Configuration;

namespace utb_eshop_patek.Application.Configuration
{
    public class ServiceConfiguration
    {
        public static void Load(IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(ServiceConfiguration)));
            InfrastructureServiceConfiguration.Load(services);
        }
    }
}
