﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Admin.ViewModels.Products
{
    public class ProductViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string ImageURL { get; set; }
    }
}
