﻿using utb_eshop_patek.Application.Admin.ViewModels.Products;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Products
{
    public interface IProductApplicationService
    {
        IndexViewModel GetIndexViewModel();
        ProductViewModel GetProductViewModel(int id);
        ProductViewModel Insert(ProductViewModel model);
        ProductViewModel Update(ProductViewModel model);
        ProductViewModel Delete(ProductViewModel model);
    }
}
