﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using utb_eshop_patek.Application.Admin.ApplicationServices.Products;
using utb_eshop_patek.Application.Admin.ViewModels.Products;

namespace utb_eshop_patek.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductsController : Controller
    {
        private readonly IProductApplicationService _productApplicationService;

        public ProductsController(IProductApplicationService productApplicationService)
        {
            _productApplicationService = productApplicationService;
        }

        public IActionResult Index()
        {
            var vm = _productApplicationService.GetIndexViewModel();
            return View(vm);
        }

        public IActionResult Edit(int id)
        {
            var product = _productApplicationService.GetProductViewModel(id);
            return View(product);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel model)
        {
            _productApplicationService.Update(model);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Insert()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Insert(ProductViewModel model)
        {
            _productApplicationService.Insert(model);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Delete(ProductViewModel model)
        {
            _productApplicationService.Delete(model);
            return RedirectToAction(nameof(Index));
        }
    }
}